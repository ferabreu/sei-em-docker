# SEI em Docker

<!-- Índice -->
[[_TOC_]]

## Descrição

Este projeto serve como base para a criação de uma Docker Stack para a execução do Sistema Eletrônico de Informações (SEI) em containers.

>*O Sistema de Eletrônico de Informações – SEI é um sistema de gestão de processos eletrônicos que garante a autuação, instrução, assinatura e o controle do trâmite dos processos em meio eletrônico desenvolvido pelo Tribunal Regional Federal da 4ª Região (TRF4).*\
>[(Manual de usuário do Sistema Eletrônico de Informações | Biblioteca Nacional)][sei-descricao-biblioteca]

>*O SEI é uma ferramenta de gestão de documentos e processos eletrônicos com interface amigável e práticas inovadoras de trabalho. Foi desenvolvido pelo Tribunal Regional Federal da 4ª Região (TRF4).*\
>[(Cartilha do Usuário - Sistema Eletrônico de Informações - Ministério da Economia.)][sei-descricao-ministerio]

## ISENÇÃO DE RESPONSABILIDADE

Este projeto é uma solução genérica, criada a partir de um plano desenvolvido para uso em um órgão público (e implantado com sucesso), mas que não mantém vínculo com nenhuma instituição. \
Este projeto não funcionará sem adaptações para o ambiente em que se pretender implantá-lo. \
Para o correto uso deste projeto, é indispensável conhecimento em Docker e Shell Linux. \
O autor não assume qualquer responsabilidade pelo uso deste projeto, ou partes dele.

## Licença

Licença MIT (ver arquivo LICENSE).

## Implementação

### Componentes

- [Debian 11 "Bullseye"][debian]
- [Apache HTTP Server 2.4][apache]
- [PHP 7.4][php]
- [Memcached 1.6][memcached]
- [Solr 8.11][solr]
- [Oracle Instant Client 8][oci]
- Java Runtime Environment (JRE) [Eclipse Temurin][eclipse-temurin], versão 8
- [wkhtmltopdf 0.12][wkhtmltopdf]
- SEI versão 4.0

### Imagens Docker utilizadas

- [php:7.4.33-apache-bullseye][php-docker] (base da imagem sei_php)
- [memcached:1.6.18-bullseye][memcached-docker]
- [solr:8.11.2][solr-docker]

### Estrutura

A stack é composta pelos seguintes serviços:
- sei_php: container baseado na imagem customizada sei_php, roda os sistemas SEI e SIP. Configurações (Docker configs e secrets) são montadas no deploy.
- sei_memcached: container baseado na imagem oficial do Memcached 1.6, sem alterações.
- sei_solr: container baseado na imagem oficial do Apache Solr 8.11. Alterações apenas no deploy: montagem do diretório contendo os "cores" de indexação do SEI.

A comunicação entre os serviços é feita através da rede interna do Docker.

A imagem sei_php, customizada a partir de uma imagem Docker oficial PHP:apache, inclui PHP 7.4 e Apache 2.4, rodando em Debian, e também:
- instalações:
   - Java Runtime Environment versão 8 - a única que funciona com o arquivo Java `pdfboxmerge.jar` incluído no SEI;
   - wkhtmltopdf 0.12 com "patched qt5", que executa corretamente um argumento invocado pelo SEI na geração de PDFs;
   - pacotes do Debian;
   - extensões do PHP (através do [install-php-extensions][install-php-extensions]) e dependências, incluindo todo o Oracle Instant Client 8.
- configurações:
   - idioma e fuso-horário do Debian;
   - cron (executa os agendamentos de tarefas de manutenção do SEI);
   - inicializador do Apache (inclui a ativação de módulos, site e configs para SSL e para o SEI, atribuição do arquivo php-ini conforme o ambiente, e a inicialização do cron).

Itens que requerem ajustes manuais, de acordo com o ambiente de implantação:
- o envio de emails pelo sistema exigirá configurações e, possivelmente, componentes adicionais na imagem sei_php;
- o acesso externo ao SEI e SIP depende de configurações, certificados de segurança, etc:
  - recomenda-se que sejam montados no deploy da stack, e não incluídos na imagem sei_php.

#### Arquivos no repositório

```
.
├── .gitlab-ci.yml
├── compose.yaml
├── compose-des.yaml
├── compose-hml.yaml
├── compose-prd.yaml
├── README.md
├── resources
│   ├── apache-default-ssl.conf # arquivos de configuracoes do Apache, definidos no build
│   ├── apache-sei.conf
│   ├── apache-ssl-params.conf
│   ├── permissoes_ctr-php.sh   # scripts para ajustes de permissoes em diretorios montados nos containers
│   ├── permissoes_ctr-solr.sh
│   ├── php-sei.ini             # arquivo de configuracao do PHP, definido no deploy
│   └── sei-cores-8.1.11.zip    # índices do SEI, modificados para uso com o Solr 8.11
└── sei_php
    ├── Dockerfile
    └── resources
        ├── robots.txt
        └── sei.cron            # arquivo para o cron rodar as tarefas do SEI
```

#### Arquivos relevantes no container sei_php

```
/
├── etc
│   ├── apache2
│   │   ├── conf-available
│   │   │   ├── sei.conf
│   │   │   └── ssl-params.conf
│   │   ├── conf-enabled
│   │   │   ├── sei.conf -> ../conf-available/sei.conf
│   │   │   └── ssl-params.conf -> ../conf-available/ssl-params.conf
│   │   ├── sites-available
│   │   │   └── default-ssl.conf
│   │   └── sites-enabled
│   │       └── default-ssl.conf -> ../sites-available/default-ssl.conf
│   ├── cron.d
│   │   └── sei
│   └── ssl
│       ├── certs
│       │   └── cert_file.crt # adapte para uso de seu certificado
│       └── private
│           └── key_file.key  # adapte para uso de sua chave
├── opt (código-fonte do sistema)
│   ├── sei
│   │   └── config
│   │       └── ConfiguracaoSEI.php # Docker secret ou config
│   └── sip
│       └── config
│           └── ConfiguracaoSip.php # Docker secret ou config
├── $PHP_INI_DIR
│   └── conf.d
│       └── sei.ini
├── var
│   └── www
│       └── html
│           └── robots.txt
└── home
    └── permissoes.sh
```

#### Arquivos relevantes no container SOLR

```
/
├── var
│   └── solr (MONTAGEM DOS CORES DO SOLR)
│       └── data
│           └── sei-*
│               ├── conf
│               │   ├── schema.xml
│               │   └── solrconfig.xml
│               ├── conteudo
│               └── core.properties
└── home
    └── permissoes.sh
```

### Variáveis, configs e segredos externos do Docker (no arquivo .gitlab-ci.yml)

#### Certificados para SSL

- CERT\_FILE: seu certificado (chave publica)
- KEY\_FILE: sua chave privada

#### Imagens Docker utilizadas

- MEMCACHED\_TAG: "1.6.17-bullseye"
- PHP\_TAG: "7.4.32-apache-bullseye"
- SOLR\_TAG: "8.11.2"

#### SEI e componentes da imagem sei_php

- SEI\_MAJ\_URL: URL do arquivo do SEI versão 4.0.0 (em um repositório da instituicao, ou outro local)
- SEI\_MAJ\_SHA256: SHA256 do arquivo do SEI versão 4.0.0
- SEI\_UPD\_URL: URL do arquivo de atualização do SEI (ex. do arquivo da versao 4.0.10)
- SEI\_UPD\_SHA256: SHA256 do arquivo de atualização do SEI
- SEI\_MOD\_TAL\_URL: URL do arquivo de um módulo qualquer (opcional)
- SEI\_MOD\_TAL\_SHA256: SHA256 do arquivo de um modulo qualquer (opcional)
- JRE\_URL: <https://github.com/adoptium/temurin8-binaries/releases/download/jdk8u362-b09/OpenJDK8U-jre_x64_linux_hotspot_8u362b09.tar.gz>
- JRE\_SHA256: c8c4e180f915fc7c163240bf363dcdf2b481cd2723fabfc3d08ccf12e049611f
- WK\_URL: <https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6.1-2/wkhtmltox_0.12.6.1-2.bullseye_amd64.deb>
- WK\_SHA256: 50a3c5334d1fb21349f8ec965fc233840026c376185e3aa75373e6e7aa3ff74d
- SEI\_TAG: versão do SEI atualizado (ex. 4.0.10)
- SEI\_PHP\_TAG: tag da imagem sei_php gerada

## Implantação

O uso de uma ferramenta de CI/CD facilita este trabalho. O projeto inclui um arquivo `.gitlab-ci.yml` com algumas sugestões. \
Para a montagem, deve-se fornecer as URLs para os arquivos do SEI e o nome da rede Docker que dê acesso ao sistema. \
Caso pretenda realizar estes passos manualmente, os comandos abaixo podem ser um ponto de partida.

### Build da imagem sei_php

```
docker build \
  --build-arg REGISTRY_PROXY= \     # URL do registro Docker da instituicao      (opcional - padrão 'docker.io/library')
  --build-arg PHP_TAG= \            # tag da imagem PHP a usar como base         (opcional - padrão '7.4.33-apache-bullseye')
  --build-arg SEI_MAJ_URL= \        # URL do arquivo do SEI versao 4.0.0         (em um repositorio da instituicao, ou outro local)
  --build-arg SEI_MAJ_SHA256= \     # SHA256 do arquivo do SEI versao 4.0.0
  --build-arg SEI_UPD_URL= \        # URL do arquivo de atualizacao do SEI       (ex.: do arquivo da versao 4.0.10)
  --build-arg SEI_UPD_SHA256= \     # SHA256 do arquivo de atualizacao do SEI
  --build-arg SEI_MOD_TAL_URL=  \   # URL do arquivo de um modulo tal            (opcional)
  --build-arg SEI_MOD_TAL_SHA256= \ # SHA256 do arquivo de um modulo tal         (opcional)
  --build-arg JRE_URL= \            # URL do arquivo do Java runtime versao 8    (opcional - incluido no Dockerfile)
  --build-arg JRE_SHA256= \         # SHA256 do arquivo do Java runtime versao 8 (opcional - incluido no Dockerfile)
  --build-arg WK_URL= \             # URL do arquivo do wkhtmltopdf              (opcional - incluido no Dockerfile)
  --build-arg WK_SHA256= \          # URL do arquivo do wkhtmltopdf              (opcional - incluido no Dockerfile)
  --cache-from "${REGISTRY}/${CI_PROJECT_PATH}/sei_php:latest" \                 (opcional)
  --network \                       # rede Docker para o SEI
  --tag "${REGISTRY}/${CI_PROJECT_PATH}/sei_php:${SEI_PHP_TAG}" \                (opcional)
  --tag "${REGISTRY}/${CI_PROJECT_PATH}/sei_php:latest" \                        (opcional)
  sei_php/ || exit 1'
```

### Deploy da stack

```
REGISTRY_PROXY= \       # URL do registro Docker da instituicao (ex. 'docker.io/library', para organizações sem um registro próprio)
 MEMCACHED_TAG= \       # tag da imagem do Memcached (ex. '1.6.18-bullseye')
 SOLR_TAG= \            # tag da imagem do Solr (ex. '8.11.2')
 CI_ENVIRONMENT_NAME= \ # ambiente em que se implanta a stack ('development', 'staging', ou 'production')
 CI_PROJECT_PATH= \     # caminho para a imagem no repositorio
 DOCKER_HOST= \         # Docker host
 CERT_FILE= \           # nome do Docker secret com o arquivo do certificado SSL (cert_file.crt)  <-- MELHORAR ISSO
 KEY_FILE= \            # nome do Docker secret com o arquivo da chave privada SSL (key_file.key) <-- MELHORAR ISSO
 SEI_CONFIG= \          # nome do Docker secret com o arquivo de configuração do SEI
 SIP_CONFIG= \          # nome do Docker secret com o arquivo de configuração do SIP
 docker stack deploy --compose-file compose.yaml -c [ARQUIVO] sei
```
ARQUIVO é o arquivo YAML apropriado para o ambiente em que se está implantando a stack: `compose-des.yaml`, `compose-hml.yaml`, ou `compose-prd.yaml`. \
O ideal é usar Docker secrets para os certificados e configurações do SEI, mas, caso se opte pelo uso de arquivos, basta incluí-los no diretório `./resources`, alterar as declarações de Docker configs no arquivo YAML a ser usado, confome desejado, e suprimir as variáveis do comando de deploy.

### Pontos críticos

- É indispensável manter corretos dono, grupo e permissões dos diretórios e arquivos montados.
- Após alterações nos arquivos de configurações, não basta realizar o deploy - o Docker não aceita atualizar configs de serviços, apenas labels. Pode ser necessário excluir a stack antes de um novo deploy.
- Para alterações em secrets, pode ser necessário excluir a stack e os secrets antes de gerar novos secrets.
- O serviço está configurado para tráfego HTTPS, e depende de certificados existentes no host do Docker. Não é possível ter tráfego HTTPS entre o cliente e o Apache externo, e HTTP entre o Apache externo e o interno do SEI.
- Chave "XSS" (cross-site scripting) na configuracao do SEI: não incluída, permanece no valor default do sistema.
- Seria ótimo montar o container sei_php através de um build multi-stage. Infelizmente, a necessidade de definir uma série de ownerships nos diretórios e arquivos do SEI complica isso - a instrução COPY do Docker não mantém entradas de ownership, e usar um chown na imagem final aumentaria seu tamanho.

### Sugestões de roteiros

O usuário deve ter acesso à documentação fornecida com o código-fonte do SEI. \
Em caso de problemas, verificar o roteiro de migração e o manual de instalação (a seção "problemas e soluções" pode ser útil).

#### Para implantação de uma nova instância do SEI

1. Ajustar as configurações e variáveis em todos os arquivos que tratam do build e do deploy: caminhos para arquivos, URLs, versões, roteamento no Docker, etc.
   - Build: 
     - `sei_php/Dockerfile` (imagem sei_php).
     - `resources/configs/apache-default-ssl.conf` (nomes de arquivos dos certificados SSL para o Apache);
   - Deploy: 
     - `.gitlab-ci.yml` (caso se pretenda usar o Gitlab-CI);
     - `compose.yaml, compose-*.yaml` (stack);
     - `resources/scripts/permissoes_host*.sh` (definição de proprietários e permissões - **executar nos hosts**, se desejado),

1. Caso necessário, incluir as definições necessárias no roteador interno utilizado pela organização.

1. Conforme as definições ajustadas previamente, e atendendo às particularidades do ambiente de implantação:
   - preencher as configurações nos arquivos de configuração do SEI e SIP;
   - copiar os _cores_ do Solr para o diretório escolhido no host;
   - organizar diretórios do repositório de arquivos e dos índices do Apache Solr, nos respectivos hosts:
     - definir proprietários, grupos e permissões (podem ser usados os scripts `resources/scripts/permissoes_host*.sh`):
       - do repositório de arquivos (o usuário 33, que corresponde ao "apache" do container, precisa de acesso de escrita):
         ```
         /diretorio_no_host       - root:33     drwxrwxr-x
          └──────────────── *     - root:33     drwxrws---
         ```
       - dos índices do Solr (o usuário 8983, que corresponde ao "solr" do container, precisa de acesso de escrita):
         ```
         /diretorio_no_host       - 8983:8983   drwxrwxr-x
          └──────────────── data  - 8983:8983   drwxrws---
         ```

1. Criar as bases de dados, e preencher os campos necessários, conforme instruções da documentação do SEI.

1. Fazer o deploy da stack.

1. Acessar os sistemas SIP e SEI e verificar parâmetros nos menus adequados, funcionamento, etc.

1. Implantação concluída.

#### Para atualização de uma instância do SEI em funcionamento, rodando da maneira convencional

1. Ajustar as configurações e variáveis em todos os arquivos que tratam do build e do deploy: caminhos para arquivos, URLs, versões, roteamento no Docker, etc.
   - Build: 
     - `sei_php/Dockerfile` (imagem sei_php).
     - `resources/configs/apache-default-ssl.conf` (nomes de arquivos dos certificados SSL para o Apache);
   - Deploy: 
     - `.gitlab-ci.yml` (caso se pretenda usar o Gitlab-CI);
     - `compose.yaml, compose-*.yaml` (stack);
     - `resources/scripts/permissoes_host*.sh` (definição de proprietários e permissões - **executar nos hosts**, se desejado),

1. Caso necessário, incluir as definições necessárias no roteador interno utilizado pela organização.

1. Conforme as definições ajustadas previamente, e atendendo às particularidades do ambiente de implantação:
   - ajustar as configurações nos arquivos de configuração do SEI e SIP;
   - organizar diretórios do repositório de arquivos e dos índices do Apache Solr, nos respectivos hosts:
     - definir proprietários, grupos e permissões (podem ser usados os scripts `resources/scripts/permissoes_host*.sh`):
       - do repositório de arquivos (o usuário 33, que corresponde ao "apache" do container, precisa de acesso de escrita):
         ```
         /diretorio_no_host       - root:33     drwxrwxr-x
          └──────────────── *     - root:33     drwxrws---
         ```
       - dos índices do Solr (o usuário 8983, que corresponde ao "solr" do container, precisa de acesso de escrita). \
         Eles podem ser construídos a partir do arquivo ZIP contendo os _cores_ do Solr, ou, caso existentes, apenas reindexados (posteriormente):
         ```
         /diretorio_no_host       - 8983:8983   drwxrwxr-x
          └──────────────── data  - 8983:8983   drwxrws---
         ```
         OBS: na implantação original, a reindexação não foi possível. Os índices foram refeitos do zero, a partir dos _cores_ no ZIP.

1. Parar o SEI em Produção:
   - parar o servidor Web;
   - suprimir agendamentos no cron;
   - parar o Memcached;
   - parar o Solr;
   - (opcional) parar o Postfix.

1. Fazer um backup das bases de dados.

1. Iniciar o Docker no host.

1. Fazer o deploy da stack. \
   Status:
   - Memcached concluído;
   - Solr rodando com índices vazios;
   - SEI não funcional.

1. Alterar, na tabela "sistema" da base SIP, as referências para o novo ambiente.

1. Os logs das operações a seguir ficarão disponíveis no container sei_php, em `/home/log/`. É possível salvá-los no host, em `/directory/sei/log`, configurando adequadamente os arquivos compose dos ambientes de implantação.

1. Rodar, a partir do container sei_php em execução, o script para atualização do sistema SIP.

1. Incluir chaves de acesso, geradas no passo anterior, nos Docker Secrets ou Configs configurados para uso na stack.

1. Reimplantar a stack. \
   Status:
   - Memcached concluído;
   - Solr rodando com índices vazios;
   - SEI com acesso ao banco de dados, porém não funcional.

1. Rodar o script para atualização dos recursos do SEI no SIP.

1. Rodar o script para atualização do sistema SEI.

1. Executar a indexação (ou reindexação) dos índices do Solr, a partir do respectivo container.

1. Atualizar configurações do servidor web externo da organização.

1. Habilitar módulos previamente incluídos na imagem sei_php.

1. Desativar agendamentos desnecessários no SEI.

1. Verificar parâmetros do SEI e SIP nos menus adequados.

1. Implantação concluída.

#### Para atualização da versão do SEI rodando em Docker

Preferencialmente, atualizar imagens e componentes junto com o SEI, para aproveitar a mesma rotina de testes.

Para atualizar o SEI:
1. Criar um novo branch no repositório (ex.: "4.0.10" ou "v4.0.10").

1. Fazer as alterações necessárias:
   - para atualizar a versão do SEI e os componentes da imagem sei_php, alterar os valores das variáveis correspondentes ("SEI e componentes da imagem sei_php") no Dockerfile;
   - para atualizar as versões das imagens Docker utilizadas, modificar os valores das variáveis anteriormente indicadas ("Imagens Docker utilizadas"). É importante fixar as versões até o último nível (ex: "7.4.33", não "7.4" ou "7"), e conferir, nas páginas das imagens, quais versões (tags) estão imunes a vulnerabilidades de segurança, como "Log4Shell" e "Text4Shell".  

1. Faça o commit e dispare a montagem da imagem php atualizada, recriando a tag "build" a partir do branch criado.

1. Faça o deploy, criando a tag "desenvolvimento" a partir do branch criado.

1. Teste.

1. Caso OK, integre o branch criado ao "main", e faça os deploys criando as tags "homologacao" e "producao".

É possível atualizar imagens e componentes de forma independente, e até mesmo sem a criação de um branch para isso. Nesse caso, build e novos deploys, em qualquer ambiente, usarão os componentes atualizados. Isso pode ser útil no caso de atualizações de segurança imprescindíveis nas imagens e componentes prontos.

## Observações

Como a imagem sei_php é grande e complexa, optou-se pela montagem de quase todos os arquivos de configurações na fase do deploy - dessa forma, é possível fazer ajustes e substituir a stack com maior rapidez. Dependendo do que for alterado, isso poderá exigir a exclusão da stack antes de um novo deploy, conforme explicado na seção "Pontos críticos", adiante). 

Montagens de segredos e dados, e definição de configurações, no deploy dos containers do Memcached e Apache Solr, viabilizam o uso das imagens oficiais destas aplicações, sem necessidade de alterações.

O arquivo `resources/sei-cores-8.11.2.zip` deste repositório possui _cores_ para o Solr, criados a partir de exemplos fornecidos na imagem v8.11.2 desta aplicação, adequados para uso na execução da stack do SEI. \
(Os cores fornecidos com o SEI 4.0.0 foram criados para o Solr 8.2.0 - há pequenas diferenças).

O painel de administração do Solr não está acessível.

## Vulnerabilidades de segurança

O Apache Solr 8.11.2 não é afetado pela vulnerabilidade Log4Shell (CVE-2021-44228). Infelizmente, [a biblioteca apache-commons-1.6.jar pode ser vulnerável à Text4Shell (CVE-2022-42889)][commons-text-security].

## Suporte

Nenhum.

## Roadmap

- Atualizar o PHP para a versão 8.
- Incluir recursos e configurações para que o SEI possa enviar emails com segurança (encriptação, certificados, etc.).

<!-- LINKS -->
[apache]: https://httpd.apache.org
[debian]: https://www.debian.org
[eclipse-temurin]: https://adoptium.net/temurin/releases
[install-php-extensions]: https://github.com/mlocati/docker-php-extension-installer
[memcached]: https://memcached.org
[memcached-docker]: https://hub.docker.com/_/memcached
[oci]: https://www.oracle.com/database/technologies/instant-client.html
[php]: https://www.php.net
[php-docker]: https://hub.docker.com/_/php
[sei-descricao-biblioteca]: https://www.bn.gov.br/producao/documentos/manual-usuario-sistema-eletronico-informacoes
[sei-descricao-ministerio]: https://www.gov.br/economia/pt-br/acesso-a-informacao/sei/comunicados/cartilha-do-usuario-do-sei
[solr]: https://solr.apache.org
[solr-docker]: https://hub.docker.com/_/solr
[wkhtmltopdf]: https://wkhtmltopdf.org
[commons-text-security]: https://commons.apache.org/proper/commons-text/security.html

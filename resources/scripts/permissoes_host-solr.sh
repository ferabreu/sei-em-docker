#!/bin/sh

# Caminho (no host) para os indices do Solr
SOLR_PATH=

echo
echo 'Definir proprietario, grupo e permissoes para diretorios e arquivos'
echo 'dos indices do Solr.'
echo
echo "Caminho: ${SOLR_PATH}"
echo

# xtrace: print commands just before execution
# - with all expansions and substitutions done, and words marked
# - useful for debugging.
set -x

# 8983: UID do usuario "solr" no container
chown --recursive 8983:8983 "${SOLR_PATH}"

# diretorios e arquivos com escrita pelo Solr
find "${SOLR_PATH}" -type d -exec chmod 2770 {} \;
find "${SOLR_PATH}" -type f -exec chmod 0660 {} \;

# xtrace off
set +x

echo
echo 'Concluido.'
echo


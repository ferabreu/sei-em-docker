#!/bin/sh

# Caminho (no host) para o repositorio de arquivos
REPO_PATH=

echo
echo 'Definir proprietario, grupo e permissoes para diretorios e arquivos'
echo 'do repositorio de arquivos.'
echo
echo "Caminho: ${REPO_PATH}"
echo

# xtrace: print commands just before execution
# - with all expansions and substitutions done, and words marked
# - useful for debugging.
set -x

# 33: UID do usuario "www-data" (Apache) no container
chown --recursive root:33 "${REPO_PATH}"

# diretorios e arquivos com escrita pelo Apache
find "${REPO_PATH}" -type d -exec chmod 2770 {} \;
find "${REPO_PATH}" -type f -exec chmod 0660 {} \;

# xtrace off
set +x

echo
echo 'Concluido.'
echo


#!/bin/sh

# Caminho (no container) para o repositorio de arquivos
REPO_PATH=/dados

echo
echo 'Definir proprietario, grupo e permissoes para diretorios e arquivos'
echo 'do repositorio de arquivos.'
echo
echo "Caminho: ${REPO_PATH}"
echo

# xtrace: print commands just before execution
# - with all expansions and substitutions done, and words marked
# - useful for debugging.
set -x

# 33: UID do usuario "www-data" (Apache) no container
chown --recursive root:33 "${REPO_PATH}"

# diretorios e arquivos com escrita pelo Apache
find "${REPO_PATH}" -type d -exec chmod 2770 {} \;
find "${REPO_PATH}" -type f -exec chmod 0660 {} \;

# xtrace off
set +x

# DESNECESSARIO - DEFINIDO NA CRIACAO DA IMAGEM
# ## SEI
# # Caminho para o codigo-fonte
# SEI_PATH=/opt

# echo
# echo 'Definir proprietario, grupo e permissoes para diretorios e arquivos'
# echo 'do codigo-fonte.'
# echo
# echo "Caminho: ${SEI_PATH}"
# echo

# # xtrace on
# set -x

# # 33: UID do usuario "www-data" (Apache) no container
# chown --recursive root:33 "${SEI_PATH}"

# # diretorios e arquivos somente leitura para o Apache
# find "${SEI_PATH}" -type d -exec chmod 2750 {} \;
# find "${SEI_PATH}" -type f -exec chmod 0640 {} \;

# # diretorios temp com escrita pelo Apache
# find "$SEI_PATH/sei/temp" -type d -exec chmod 2770 {} \;
# find "${SEI_PATH}/sip/temp" -type d -exec chmod 2770 {} \;

# # xtrace off
# set +x

## FIM
echo
echo 'Concluido.'
echo

